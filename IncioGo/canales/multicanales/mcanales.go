package main

import (
	"fmt"
	"time"
)

func genera(salida chan<- int) {
	for i := 0; i < 5; i++ {
		salida <- i
	}
	close(salida)
}

func cuadrado(entrada <-chan int, salida chan<- int) {
	for i := range entrada {
		time.Sleep(500 * time.Millisecond)
		salida <- i * i
	}
	close(salida)
}
func main() {
	numeros := make(chan int)
	cuadrados := make(chan int)
	cuartas := make(chan int)

	go genera(numeros)
	go cuadrado(numeros, cuadrados)
	go cuadrado(cuadrados, cuartas)

	for i := range cuartas {
		fmt.Printf("%d", i)
	}
	fmt.Println()
}
