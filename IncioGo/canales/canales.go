package main

import (
	"fmt"
)

/**
Canales son conductos donde podemos enviar valores entre goroutines
*/
func main() {
	fmt.Println("simple channel")
	// define a channel and el data type
	c := make(chan int)
	dobles := make(chan int)

	// run a function in background
	go func() {
		fmt.Println("goroutine process")
		c <- 10 // write data to a channel
	}()
	val := <-c // read data from a channel
	fmt.Printf("value: %d\n", val)

	go func() {
		for i := 0; i < 5; i++ {
			dobles <- doble(i)
		}
		close(dobles)
	}()
	//for infinito
	for i := range dobles {
		fmt.Printf("%d ", i)
	}
	fmt.Println()

}

func doble(val int) int {
	return val * 2
}
