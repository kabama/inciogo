package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	writeFile("Vamos colombia!!")
	readFile()
}

func writeFile(message string) {
	bytes := []byte(message)
	ioutil.WriteFile("C:/Users/kaleth.bahena/Documents/kbm/go/tour/src/archivos/testgo.txt", bytes, 0644)
	fmt.Println("created a file")
}

func readFile() {
	data, _ := ioutil.ReadFile("C:/Users/kaleth.bahena/Documents/kbm/go/tour/src/archivos/testgo.txt")
	fmt.Println("file content:")
	fmt.Println(string(data))
}
