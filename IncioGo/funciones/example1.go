package main

import "fmt"

func main() {
	fmt.Printf("%d", add(4, 5, 6, 7))
	closureFun("Kaleth Bahena")
	fmt.Printf("Fibo() = %d", fibo(7))
}

func add(numbers ...int) int {
	result := 0
	for _, number := range numbers {
		result += number
	}
	return result
}

/** Closure Function */

func closureFun(name string) {
	hoo := func(a, b int) {
		result := a * b
		fmt.Printf("hoot() = %d  \n", result)
	}

	joo := func(a, b int) int {
		return a*b + a
	}
	fmt.Printf("closure_func(%s) was called\n", name)
	hoo(2, 3)
	val := joo(5, 8)
	fmt.Printf("val from joo() = %d \n", val)

}

/** Recursion*/
func fibo(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	}

	return (fibo(n-1) + fibo(n-2))
}
