package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Area de un circulo")
	fmt.Print("Ingresa un valor del radio: ")
	var radius float64
	fmt.Scanf("%f", &radius)

	area := math.Pi * math.Pow(radius, 2)
	fmt.Printf("Area del circulo con radio %.2f = %.2f \n", radius, area)
}
