package main

import (
	"fmt"
	"math/rand"
)

var myvar string
var n, m int
var mn float32

var (
	name  string = "Kaleth Bahena"
	email string = "kbmplaying@hotmail.com"
	age   int    = 36
)

func main() {
	city := "Bogotá"
	fmt.Println("My favorite number is", rand.Intn(15))
	fmt.Println("My nombre es", name)
	fmt.Println("My email es", email)
	fmt.Println("My edad es", age)
	fmt.Println("My ciudad es", city)
}
